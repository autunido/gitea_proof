This is an OpenPGP proof that connects my [OpenPGP key](https://keyoxide.org/wkd/autunido@senkals.one) to [this Codeberg.org account](https://codeberg.org/autunido). 
For details check out [Keyoxide.org](https://keyoxide.org/).
